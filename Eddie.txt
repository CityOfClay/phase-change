phasechange

It was a lonely gas station, set beside a country crossroads surrounded by forest and cornfields.  Its walls were peeling cinder block and the floor was a filthy bare cement slab.  Rows of grey hardware store shelving sported room-temperature snack foods.  Everyone was gone for the evening, except my brother Eddie and me, waiting for our catch to come in. 

Eddie parked his butt on a plastic five gallon bucket and scratched under his choker necklace, one whose sole purpose was to cover up a snail tattoo on his neck.  He pulled two cans of beer from a cooler, opened them and and handed one to me.  

The sun was low in the Summer sky and even bugs on the pavement outside were casting long shadows through the open doorway.  'Andy' should be here pretty soon. 

. . .

So: people die.  They die doing what they did in life, running, swimming, pooping, screwing, whatever.  Inevitably a percentage die driving.  

Recursive Engineering Inc. had sold services for millions of self-driving cars before it was sued out of existence, so for them that percentage had become pretty impressive.  Some drivers were old and went of natural causes, some choked on food, or committed suicide or OD'd.  And when this happened, they would go limp at the controls, causing the car's software to assume they had dozed off and let self-driving mode take over. 

That was the first bug.  The second was that rather than stop at a destination, the car would just keep driving, touching on all the familiar places the deceased person had routinely visited in their particular life.  These rolling coffins never stopped, as long as there was money in the owner's account to robofuel or charge at gas stations. It didn't help that tintable windows were a fashion now.  

This might have been an innocent mistake in the beginning but RE had no incentive to fix it.  As long as their customers were still legally alive and driving around, Recursive still collected billions in revenue on their service.  In court it was revealed that the bug had been pointed out internally but ignored, so RE lost everything and was convicted of massive fraud, improper disposal of human remains and Felony Carbon Waste.

Authorities seized control of course and ordered all the wayward vehicles home to police stations for next of kin to identify, but RE's database was as corrupt as their ethics and a large number of those cars simply fell off the grid.  

The public was horrified and reacted immediately.  Bumper stickers read: 'Friends don't let friends drive dead', or the ever popular 'If you can read this sign then you are alive'.  Eddie had one of those himself, on his deep-sea blue Ford Tesla.  

A rock band named Clown Car of Death achieved some success.  Their logo was a cartoon speeding car with dead clowns hanging out of all the windows.  Quite a lot of dead clowns actually, for such a tiny car.  

I will never look at headlights in my rear view mirror the same way again.  

That, then, is where our money-making opportunity began, and money we did need.  Eddie had worked for RE as a programmer and while he had escaped prosecution he suddenly found himself without a job or even a career. He had expertise on this subject though, and with such a large mess to clean up, people were ready to pay without too many questions. I myself was in between jobs and marriages, and ready to join anything.  

I did ask him once what he had seen back when he worked for Recursive.  He just burped out the words 'gag order' through his bong hit, causing him to actually gag.  He looked a little angry so I didn't ask again.  

. . . 

Our sunny morning had begun with a voicemail:  

'There's a flyblown mess that cruises through our neighborhood twice a week.  I can't get the city to do anything about it.  Kids are playing here!  They call it Maggoty Andy and make up stories about it.  They seemed like such normal kids before.  Please get rid of this thing, we'll pay cash.'  

Maggoty Andy is actually a pretty common name these days.  Everybody thinks they're clever, but we've picked up at least ten Andys.  We've heard other names... 

La Abuelita Seca was our favorite customer, an elderly woman who drove very politely and kept her air conditioner high and dry.  She mummified completely, no mess.  I respect that. 

Mazarotten was one who got away.  It was a retro-chrome chartreuse Evantra owned by a hobby racer, which became famous when she got stuck on a community race track, running other cars off the course. Sadly she crashed and burned on the wall before taking the cup.  Cremation may be dignified, but it didn't pay our rent. 

The Traveling Salesman Problem was our hardest case. TTSP's real name was Masoud, a beloved four-star Kirby vacuum cleaner salesman, extreme extrovert, and maybe a little manic.  His regular path was all over damn North America, in a pattern so complex that by the time we caught him, he was just bones rolling around the footwell under his gas pedal.  

So many stories, but even I think Topps went a bit too far with the cards.  

. . .

We heard tires on gravel and I checked my watch.  Outside, sure enough our Andy was coming up the road.  Right make and model, and if there was any doubt left you could also see an arm hanging out the window, flopping around with a few fingers missing, slapping flies away at every bump.  

It's tempting to throw a net over the whole mess as soon as you see it, but these things can be evasive.  They're made to pick up the driving style of their owners, within the law.  Want to get there fast and don't mind sudden starts and stops?  Or do you prefer a smooth ride and willing to take your time?  Just drive a little and your car will copy you.  Anyway some cars really are jerks.  Some react like they're being carjacked, and will *almost* run you down.  So we stay patient, and wait for them to stop and connect to the pump before making any moves.  

Andy's car was a two tone Volkswagen Baja Humbug, yellow fading into a deep caramel brown, with embedded flakes of gold in the finish.  I admired its beauty until it came close enough to smell.  The pump reached out, pecked its hose around like a bird until it found the open gas port and began to fuel.  It looked so maternal.  

Right then we rushed to shove a power jack under the front bumper and lift its drive wheels off the ground, but the VW was having none of that.  It backed up suddenly, tearing the pump nozzle loose in a splatter of gasoline, and then shot backward, running over my foot, to crash into the front of our tow truck.  I have *never* seen that before, auto-drives are fanatically safe, but whatever, we were now kind of screwed.  The car had no reason to stop.  We tried to corral it with our bodies, and while it seemed unwilling to hit us directly, we were in a stalemate.  Eddie was forced to do the unthinkable: he dove through the driver side window on top of the reeking glop that was 'Andy' and pulled out the keys.  My hero!  But, eww. 

Per ritual, we removed the battery and front wheels, sealed the windows and doors with tape and biohazard stickers and winched it onto the back of our tow truck.  A closer look revealed that the rear sensors were cracked and the inspection sticker had been hand-drawn with magic marker, a long time ago.  What a slob. 

Back inside, Eddie sat down on the bucket and looked really tired.  

'You know the best thing about this job?  It ends.  Every time we catch one the numbers go down, not up.  Someday we'll catch the last one, and this whole mess will be over.'  

I grabbed the rest room keys and went out in the dark for a piss.  When I was done and came around front there was Eddie's car, the blue Ford Tesla, moving slowly over the weedy gravel.  A shadow inside confirmed that someone must be stealing it.  I ran up and pounded on the driver side window to get a look at this asshole, but he just slumped over sideways, hitting his bare teeth against the glass.  A black choker necklace slipped down his mummified neck, revealing a snail tattoo.  The car picked up speed and pulled away from me, bumping over clumps of grass before it hit the road.  As its red tail lights dwindled into a black spiky horizon of trees beneath the sunset, I raced back toward the yellow glow of the gas station door, screaming Eddie's name.  

In the corner of my eye, a second set of headlights began to twinkle through the distant forest from the opposite end of the dirt road.  

***

Prunella  Abuela Seca  Abuelaseca  Mum 
Souper Man
Doctor Bones
La Podridacita

Speed Racer
Super Flies
Maserati Maserotten Maza Rotten
The Traveling Salesman Problem
The Flying Salesman?
VW Baja Humbug

There was a sticker for a rock band(?) that said 'Clown Car of Death', in front of a drawing of a speeding car with dead clowns hanging out of all the windows.  Quite a lot of dead clowns actually, for such a tiny car.

'You know the best thing about this job?  It ends.  Every time we catch a body the numbers go down, not up.  Someday we'll catch the last one, and this whole mess will be over.'  

I went out for a piss. When I was done and came around front there was Eddie's car, a sky-blue Ford Tesla, moving slowly over the weedy gravel.  A shadow inside confirmed that someone was stealing it.  I ran up and pounded on the driver side window to get a look at this asshole, but he just slumped over sideways, hitting his bare teeth against the glass.  A black choker necklace slipped down his mummified neck, revealing a snail tattoo.  The car picked up speed and pulled away from me, bumping over clumps of grass before it hit the road.  As its red tail lights dwindled into a black spiky horizon of trees beneath the sunset, I raced back toward the yellow glow of the gas station door, screaming Eddie's name.  

In the corner of my eye, a second set of headlights began to twinkle through the distant forest from the opposite end of the dirt road.  


He sat on a five gallon bucket, put down a six pack of cold beer, opened two and handed one to me.  We waited for Maggoty Andy.

In spite of the heat, my brother Eddie came into the room wearing a choker neclace, which covered up an old snail tattoo he no longer liked on is neck.

He looked around the run-down gas station,

Eddie sported one of those on his own car, a crystal blue Ford Tesla. 


// was a company that

People die.  They die doing whatever they do in life, running, swimming, pooping, screwing, whatever.  So inevitably a percentage die driving.  

Recursive Engineering had sold services for self-driving cars before it was sued out of existence.  They had millions of customers of all ages, so it was inevitable that some of them would die behind the wheel.  Some were old and went of natural causes, some choked on food, or even committed suicide or OD'd on drugs and booze.  And when this happened, the driver would go limp at the controls, causing RE's software to assume they had dozed off and let self-driving mode take over. 

// Whenever a driver's hands went limp on the wheel RE's software assumed they had gone to sleep and let self-driving mode take over. 

That was the first bug.  The second was that instead of stopping at the next destination, the cars would just keep driving, touching on all the familiar places the deceased person routinely visited in their life.  So the 'corpse cars' never stopped, as long as there was money in the owner's accounts to robofuel (or robocharge) at gas stations. 

// or full-service

This might have been a mistake in the beginning but RE had no incentive to fix it.  As long as their customers were still legally alive and driving around, RE still collected millions of dollars in revenue on their service.  In court it was revealed that the bug had been pointed out internally but ignored, so RE lost everything and was convicted of massive fraud, improper disposal of human remains and Felony Carbon Waste.

The courts seized RE's database and broadcasted for all nonresponsive customer vehicles to line up at police stations for inspection and body removal,

Shutting it all down

It is estimated that up to 30,000

Among other features was their 'safe snooze'

Eddie raced in with a power jack, to shove it under the front axle and lift the drive wheels off the ground.  The car however 

Eddie and I pulled the steel cable net around the front and back of the vehicle as 

We held our place until he pulled 
Don't react yet.  
Best to wait until he's at the pump.  

Mazarotten was one who got away.  It was a sweet <color and make> sports car owned by a hobby racer, which became famous when it got stuck on a community race track and couldn't escape, running other cars off the course. Sadly it crashed and burned on the wall before taking the cup.  Cremation may be dignified, but we never got paid.  // she cost us our reward money. 


The public was horrified and reacted immediately.  Bumper stickers read: 'If you can read this sign then you are alive', or the ever popular 'Friends don't let friends drive dead'.  Eddie had one of those himself.  
